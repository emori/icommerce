package com.abc.config.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableConfigurationProperties
@EnableConfigServer
@RefreshScope
public class ConfigServiceApplication {

	private static final Logger log = LoggerFactory.getLogger(ConfigServiceApplication.class);
	private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ConfigServiceApplication.class);
		addDefaultProfile(app);
		// app.setAdditionalProfiles("composite");
		Environment env = app.run(args).getEnvironment();
		logApplicationStartup(env);
	}

	public static void addDefaultProfile(SpringApplication app) {
		Map<String, Object> defProperties = new HashMap<>();
		/*
		 * Default values to support to run project in local
		 */
		defProperties.put("ICOMMERCE_SYS_A", "iCommercekei");
		defProperties.put("ICOMMERCE_SYS_S", "ICOMMERCE@ICOMMCERCEBANKEND2CSRKEI");
		defProperties.put(SPRING_PROFILE_DEFAULT, "dev");
		app.setDefaultProperties(defProperties);
	}
	
	private static void logApplicationStartup(Environment env) {
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String serverPort = env.getProperty("server.port");
        String contextPath = env.getProperty("server.servlet.context-path");
        if (StringUtils.isBlank(contextPath)) {
            contextPath = "/";
        }
        String hostAddress = "localhost";	
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\t{}://localhost:{}{}\n\t" +
                "External: \t{}://{}:{}{}\n\t" +
                "Profile(s): \t{}\n----------------------------------------------------------",
            env.getProperty("info.id"),
            protocol,
            serverPort,
            contextPath,
            protocol,
            hostAddress,
            serverPort,
            contextPath,
            env.getActiveProfiles());
    }
}
