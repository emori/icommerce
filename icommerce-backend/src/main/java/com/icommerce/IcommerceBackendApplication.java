package com.icommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

import com.icommerce.config.ApplicationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ ApplicationProperties.class})
@EnableZuulProxy 
public class IcommerceBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IcommerceBackendApplication.class, args);
	}

}
