package com.icommerce.security.dto;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfile  {

	private Long id;
	private String alias;
	private String username;
	private Boolean firstLogin;
	private Boolean activated;
	private String langKey;
	private Integer loginFailureCount;
	private Timestamp loginFailureDate;
	private Set<String> roleTypes = new HashSet<>();
	private String imageUrl;
	private List<String> authorities = new ArrayList<>();

	public UserProfile() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return this.alias;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the firstLogin
	 */
	public Boolean getFirstLogin() {
		return this.firstLogin;
	}

	/**
	 * @param firstLogin the firstLogin to set
	 */
	public void setFirstLogin(Boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	/**
	 * @return the activated
	 */
	public Boolean getActivated() {
		return this.activated;
	}

	/**
	 * @param activated the activated to set
	 */
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	/**
	 * @return the langKey
	 */
	public String getLangKey() {
		return this.langKey;
	}

	/**
	 * @param langKey the langKey to set
	 */
	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	/**
	 * @return the loginFailureCount
	 */
	public Integer getLoginFailureCount() {
		return this.loginFailureCount;
	}

	/**
	 * @param loginFailureCount the loginFailureCount to set
	 */
	public void setLoginFailureCount(Integer loginFailureCount) {
		this.loginFailureCount = loginFailureCount;
	}

	/**
	 * @return the loginFailureDate
	 */
	public Timestamp getLoginFailureDate() {
		return this.loginFailureDate;
	}

	/**
	 * @param loginFailureDate the loginFailureDate to set
	 */
	public void setLoginFailureDate(Timestamp loginFailureDate) {
		this.loginFailureDate = loginFailureDate;
	}

	

	/**
	 * @return the roleTypes
	 */
	public Set<String> getRoleTypes() {
		return this.roleTypes;
	}

	/**
	 * @param roleTypes the roleTypes to set
	 */
	public void setRoleTypes(Set<String> roleTypes) {
		this.roleTypes = roleTypes;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return this.imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}


	/**
	 * @return the authorities
	 */
	public List<String> getAuthorities() {
		if (!CollectionUtils.isEmpty(this.authorities)) {
			Collections.sort(this.authorities);
		}
		return this.authorities;
	}

	/**
	 * @param authorities the authorities to set
	 */
	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	@JsonIgnore
	public Collection<SimpleGrantedAuthority> getGrantedAuthority() {
		Collection<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();
		if (!CollectionUtils.isEmpty(this.authorities)) {
			for (String authority : this.authorities) {
				grantedAuthorities.add(new SimpleGrantedAuthority(authority));
			}
		}
		return grantedAuthorities;
	}

	

}
