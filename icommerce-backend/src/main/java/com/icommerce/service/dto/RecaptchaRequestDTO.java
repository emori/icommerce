package com.icommerce.service.dto;

public class RecaptchaRequestDTO {
	private String captchaResponse;
	private String remoteAddress;
	public String getCaptchaResponse() {
		return captchaResponse;
	}
	public void setCaptchaResponse(String captchaResponse) {
		this.captchaResponse = captchaResponse;
	}
	public String getRemoteAddress() {
		return remoteAddress;
	}
	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}
	
}
