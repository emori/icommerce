package com.icommerce.service.dto;

import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RecaptchaResponseDTO {
	private Boolean success;
	@JsonProperty("challenge_ts")
	private Instant challengeTs;
	private String hostname;
	@JsonProperty("error-codes")
	private List<Object> errorCodes;
	@JsonProperty("apk_package_name")
	private String apkPackageName;
	private Double score;
	private String action;

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Instant getChallengeTs() {
		return challengeTs;
	}

	public void setChallengeTs(Instant challengeTs) {
		this.challengeTs = challengeTs;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public List<Object> getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(List<Object> errorCodes) {
		this.errorCodes = errorCodes;
	}

	public String getApkPackageName() {
		return apkPackageName;
	}

	public void setApkPackageName(String apkPackageName) {
		this.apkPackageName = apkPackageName;
	}

}
