package com.icommerce.service.reposiroty;

import org.springframework.data.jpa.repository.JpaRepository;

import com.icommerce.service.domain.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
