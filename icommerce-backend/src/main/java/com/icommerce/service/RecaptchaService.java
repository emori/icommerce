package com.icommerce.service;

import com.icommerce.service.dto.RecaptchaRequestDTO;
import com.icommerce.service.dto.RecaptchaResponseDTO;

public interface RecaptchaService {

	public RecaptchaResponseDTO validateRecaptcha(RecaptchaRequestDTO request);
}
