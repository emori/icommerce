package com.icommerce.service.impl;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icommerce.config.ApplicationProperties;
import com.icommerce.config.ObjectToUrlEncodedConverter;
import com.icommerce.config.ProxyCustomizer;
import com.icommerce.service.RecaptchaService;
import com.icommerce.service.dto.RecaptchaRequestDTO;
import com.icommerce.service.dto.RecaptchaResponseDTO;

@Service
public class RecaptchaServiceImpl implements RecaptchaService {
	private final ApplicationProperties applicationProperties;
	private final RestTemplate restTemplate;
	private final ProxyCustomizer proxyCustomizer;
	private final ObjectMapper objectMapper;

	public RecaptchaServiceImpl(ApplicationProperties applicationProperties, RestTemplate restTemplate,
			ObjectProvider<ProxyCustomizer> proxyCustomizerProvider, ObjectMapper objectMapper) {
		super();
		this.applicationProperties = applicationProperties;
		this.restTemplate = restTemplate;
		this.proxyCustomizer = proxyCustomizerProvider.getIfAvailable();
		this.objectMapper = objectMapper;
	}

	@Override
	public RecaptchaResponseDTO validateRecaptcha(RecaptchaRequestDTO request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		Map<String, String> map = new HashMap<>();
		map.put("secret", applicationProperties.getRecaptcha().getSiteSecret());
		map.put("response", request.getCaptchaResponse());
		map.put("remoteip", request.getRemoteAddress());
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);
		RestTemplate restTemplateLocal = restTemplate;

		if (applicationProperties.getNetwork().getProxyEnable()) {

			HttpClientBuilder clientBuilder = HttpClientBuilder.create();
			this.turnOnProxy(clientBuilder, applicationProperties);
			try {
				restTemplateLocal = this.turnOffSsl(clientBuilder);
				restTemplateLocal.getMessageConverters().add(new ObjectToUrlEncodedConverter(objectMapper));
			} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		ResponseEntity<RecaptchaResponseDTO> response = restTemplateLocal.exchange(
				applicationProperties.getRecaptcha().getSiteVerifyUrl(), HttpMethod.POST, entity,
				RecaptchaResponseDTO.class);
		return response.getBody();
	}

	protected void turnOnProxy(HttpClientBuilder clientBuilder, ApplicationProperties applicationProperties) {
		HttpHost host = new HttpHost(applicationProperties.getNetwork().getProxyUrl(),
				applicationProperties.getNetwork().getProxyPort());
		clientBuilder.useSystemProperties();
		clientBuilder.setProxy(host);
	}

	protected RestTemplate turnOffSsl(HttpClientBuilder clientBuilder)
			throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());

		CloseableHttpClient httpClient = clientBuilder.setSSLSocketFactory(csf).build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);

		return new RestTemplate(requestFactory);
	}

}
