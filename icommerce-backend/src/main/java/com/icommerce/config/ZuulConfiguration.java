package com.icommerce.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfiguration {
	@Bean
	public ZuulAuditPreFilter zuulAuditPreFilter() {
		return new ZuulAuditPreFilter();
	}

	@Bean
	public ZuulCorsPostFilter zuulCorsPostFilter() {
		return new ZuulCorsPostFilter();
	}

	@Bean
	public ZuulProductServicePreFilter zuulProductServicePreFilter() {
		return new ZuulProductServicePreFilter();
	}
}
