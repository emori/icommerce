package com.icommerce.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class ZuulCorsPostFilter  extends ZuulFilter {
		
		public int filterOrder() {
		  return 1;
		}

		public boolean shouldFilter() {
		  return RequestContext.getCurrentContext().getZuulResponseHeaders().stream()
		   .anyMatch(ssp -> ssp.first().toLowerCase().equalsIgnoreCase("access-control-allow-origin")) &&
		    RequestContext.getCurrentContext().getOriginResponseHeaders().stream()
		   .anyMatch(ssp -> ssp.first().toLowerCase().equalsIgnoreCase("access-control-allow-origin"));
		}

		public Object run() throws ZuulException {
		  RequestContext.getCurrentContext().getZuulResponseHeaders()
		    .removeIf(ssp -> ssp.first().toLowerCase().startsWith("access-control-allow"));

		  return null;
		}

		@Override
		public String filterType() {
			return "post";
		}
}
