package com.icommerce.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

	private final NetworkConfiguration network = new NetworkConfiguration();
	private final RecaptchaConfiguration recaptcha = new RecaptchaConfiguration();
	
	public class SimDataProviderConfiguration {
		private String apiURL;
		private String merchantName;
		private String authenticationCode;
		public String getApiURL() {
			return apiURL;
		}
		public void setApiURL(String apiURL) {
			this.apiURL = apiURL;
		}
		public String getMerchantName() {
			return merchantName;
		}
		public void setMerchantName(String merchantName) {
			this.merchantName = merchantName;
		}
		public String getAuthenticationCode() {
			return authenticationCode;
		}
		public void setAuthenticationCode(String authenticationCode) {
			this.authenticationCode = authenticationCode;
		}
	}
	private String appName;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public NetworkConfiguration getNetwork() {
		return network;
	}

	public RecaptchaConfiguration getRecaptcha() {
		return recaptcha;
	}

	public class RecaptchaConfiguration {
		private Boolean enable;
		private String siteVerifyUrl;
		private String siteSecret;

		public Boolean getEnable() {
			return enable;
		}

		public void setEnable(Boolean enable) {
			this.enable = enable;
		}

		public String getSiteVerifyUrl() {
			return siteVerifyUrl;
		}

		public void setSiteVerifyUrl(String siteVerifyUrl) {
			this.siteVerifyUrl = siteVerifyUrl;
		}

		public String getSiteSecret() {
			return siteSecret;
		}

		public void setSiteSecret(String siteSecret) {
			this.siteSecret = siteSecret;
		}

	}

	public class NetworkConfiguration {
		private Boolean proxyEnable;
		private String proxyUrl;
		private Integer proxyPort;

		public Integer getProxyPort() {
			return proxyPort;
		}

		public void setProxyPort(Integer proxyPort) {
			this.proxyPort = proxyPort;
		}

		public Boolean getProxyEnable() {
			return proxyEnable;
		}

		public void setProxyEnable(Boolean proxyEnable) {
			this.proxyEnable = proxyEnable;
		}

		public String getProxyUrl() {
			return proxyUrl;
		}

		public void setProxyUrl(String proxyUrl) {
			this.proxyUrl = proxyUrl;
		}

	}
}
