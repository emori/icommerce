package com.icommerce.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.icommerce.security.SecurityUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class ZuulProductServicePreFilter extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(ZuulProductServicePreFilter.class);

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		String url = RequestContext.getCurrentContext().getRequest().getRequestURL().toString();
		return url.contains("/product-service/");
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));
		Map<String, List<String>> newParameterMap = new HashMap<>();
		Map<String, String[]> parameterMap = request.getParameterMap();
		// getting the current parameter
		for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			newParameterMap.put(key, Arrays.asList(values));
		}
		
		newParameterMap.put("username", Arrays.asList(SecurityUtils.getCurrentUserLogin().get()));
		ctx.setRequestQueryParams(newParameterMap);
		return null;
	}

}
