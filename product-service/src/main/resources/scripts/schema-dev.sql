DROP TABLE PRODUCT;
CREATE TABLE PRODUCT (
ID NUMBER(17,0) NOT NULL AUTO_INCREMENT,
PRODUCT_CODE VARCHAR2(20) NOT NULL,
PRODUCT_NAME NVARCHAR2(255) NOT NULL,
BRAND VARCHAR2(255) NOT NULL,
CREATED_DATE DATE,
CREATED_BY VARCHAR2(20),
LAST_MODIFIED_DATE DATE,
LAST_MODIFIED_BY VARCHAR2(20),
PRIMARY KEY (ID));

DROP TABLE PRODUCT_PRICE;

CREATE TABLE PRODUCT_PRICE (
ID NUMBER(17,0) NOT NULL AUTO_INCREMENT,
PRODUCT_ID NUMBER(17,0) NOT NULL,
PRICE NUMBER(17,2) NOT NULL,
CREATED_DATE DATE,
CREATED_BY VARCHAR2(20),
LAST_MODIFIED_DATE DATE,
LAST_MODIFIED_BY VARCHAR2(20),
VALID_FLAG VARCHAR2(1) NOT NULL DEFAULT 1,
PRIMARY KEY (ID),
CONSTRAINT fk_product_price_product_id
  FOREIGN KEY (PRODUCT_ID)
  REFERENCES PRODUCT (ID)
  );
  
INSERT INTO TABLE PRODUCT (ID, PRODUCT_CODE, PRODUCT_NAME) VALUES (1, 'PS5', 'PS5','SONY');
INSERT INTO TABLE PRODUCT (ID, PRODUCT_CODE, PRODUCT_NAME) VALUES (2, 'XBOX One', 'XBOX1','Mircrosoft');
INSERT INTO TABLE PRODUCT (ID, PRODUCT_CODE, PRODUCT_NAME) VALUES (3, 'Switch', 'Switch','Nitendo');


INSERT INTO TABLE PRODUCT_PRICE (ID, PRODUCT_ID, PRICE, VALID_FLAG) VALUES (1, 1, 200000000, '1');
INSERT INTO TABLE PRODUCT_PRICE (ID, PRODUCT_ID, PRICE, VALID_FLAG) VALUES (2, 1, 210000000, '2');
INSERT INTO TABLE PRODUCT_PRICE (ID, PRODUCT_ID, PRICE, VALID_FLAG) VALUES (3, 2, 150000000, '1');
INSERT INTO TABLE PRODUCT_PRICE (ID, PRODUCT_ID, PRICE, VALID_FLAG) VALUES (4, 3, 170000000,'1');

CREATE SEQUENCE PRODUCT_ID_SEQ
  MINVALUE 4
  MAXVALUE 99999999999
  START WITH 1
  INCREMENT BY 100
  CACHE 100;
  
CREATE SEQUENCE PRODUCT_PRICE_ID_SEQ
  MINVALUE 1
  MAXVALUE 99999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 100;  
  