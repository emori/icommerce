package com.icommerce.product.service.rest.vm;

import java.util.List;

public class ProductSearchResponse<T> {

	List<T> products;
	Pagination pagination;
	
	public List<T> getProducts() {
		return products;
	}

	public void setProducts(List<T> products) {
		this.products = products;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
