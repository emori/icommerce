package com.icommerce.product.service.constants;

import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final String CONST_STR_CONTENT_TYPE_JSON = "application/json";
	public static final String CONST_STR_UTF8 = "utf-8";
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String SEARCH_PRODUCT_NAME = "productName";
	public static final String SEARCH_PRODUCT_CODE = "productCode";
	public static final String SEARCH_BRAND = "brand";
	public static final String SEARCH_ORDER_BY = "orderBy";
	public static final String SEARCH_ORDER_TYPE = "orderType";
	public static final String SEARCH_ORDER_DESC = "DESC";
	public static final String SEARCH_ORDER_ASC = "ASC";
	public static Map<String, String> SEARCH_COLUMNS_MAPING_MAP;
	public static final String SEARCH_SIZE = "size";
	public static final String SEARCH_PAGE = "page";
	public static final String SEARCH_FROM_PRICE = "fromPrice";
	public static final String SEARCH_TO_PRICE = "toPrice";
	public static final String VALID_FLAG_VALID = "1";
	public static final String VALID_FLAG_INVALID = "2";
	static {
        SEARCH_COLUMNS_MAPING_MAP = new HashMap<>();
        SEARCH_COLUMNS_MAPING_MAP.put("product_name", "product.productName");
        SEARCH_COLUMNS_MAPING_MAP.put("product_code", "product.productCode");
        SEARCH_COLUMNS_MAPING_MAP.put("brand", "product.branch");
    }
}
