package com.icommerce.product.service.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.icommerce.product.service.logging.RestTemplateLoggingInterceptor;

@Configuration
public class JacksonConfiguration {
    
    @Bean
    RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    	ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory());
    	RestTemplate restTemplate = new RestTemplate(factory);
    	 restTemplate.setInterceptors(Collections.singletonList(new RestTemplateLoggingInterceptor()));
         MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
         ObjectMapper objectMapper = new ObjectMapper();
         JavaTimeModule javaTimeModule = new JavaTimeModule();
         objectMapper.registerModule(javaTimeModule);
         objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
         objectMapper.disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS);
         jsonConverter.setObjectMapper(objectMapper);
         List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
         converters.add(jsonConverter);
         converters.add(new ObjectToUrlEncodedConverter(objectMapper));
         restTemplate.setMessageConverters(converters);
         return restTemplate;
    }
   @Bean
   @ConditionalOnProperty( value="application.network.proxy-enable", 
		    havingValue = "true",matchIfMissing = false )
   ProxyCustomizer proxyCustomizer(ApplicationProperties applicationProperties) {
	   return new ProxyCustomizer(applicationProperties.getNetwork().getProxyUrl(), applicationProperties.getNetwork().getProxyPort());
   }
}
