package com.icommerce.product.service.exception;


import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;


public class ApplicationException extends BaseException
{
    /**
	 * 
	 */
    private static final long serialVersionUID = -5496100867391321688L;

    private int errorCode = 500;
    
    private String messageCode = null;

    private Object[] messageParams = null;

    private Object data;
    
    
    


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public int getErrorCode() {
		return errorCode;
	}


	public String getMessageCode()
    {
        return messageCode;
    }


    public Object[] getMessageParams()
    {
        return messageParams;
    }


    public ApplicationException()
    {
        super();
    }


    public ApplicationException(int errorCode, String messageCode, Object[] messageParams) {
    	super(messageCode, null);
		this.errorCode = errorCode;
		this.messageCode = messageCode;
		this.messageParams = messageParams;
	}

    public ApplicationException(int errorCode, String messageCode, Object[] messageParams, Throwable throwable) {
    	super(messageCode, throwable);
		this.errorCode = errorCode;
		this.messageCode = messageCode;
		this.messageParams = messageParams;
	}
    public ApplicationException(int errorCode, String messageCode, Object[] messageParams, Object data, Throwable throwable) {
    	super(messageCode, throwable);
		this.errorCode = errorCode;
		this.messageCode = messageCode;
		this.messageParams = messageParams;
		this.data = data;
	}


	public ApplicationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
        messageCode = arg0;
    }


    public ApplicationException(String arg0)
    {
        super(arg0);
        messageCode = arg0;
    }


    public ApplicationException(String errorCode, String errorMessage)
    {
        messageCode = errorMessage;
        setExceptionID(errorCode);
    }


    public ApplicationException(String arg0, Throwable arg1, Object[] params)
    {
        super(arg0, arg1);
        messageParams = params;
        messageCode = arg0;
    }


    public ApplicationException(String arg0, Object[] params)
    {
        super(arg0);
        messageParams = params;
        messageCode = arg0;
    }


    private String printParams()
    {
        if (messageParams == null)
        {
            return null;
        }
        String s = "";
        for (Object b : messageParams)
        {
            s += "params :" + (b == null ? "null" : b.toString()) + "\n";
        }
        return s;
    }


    @Override
    public void printStackTrace()
    {
    	System.out.println("error code:" + errorCode + " ");
        System.out.println("message code:" + messageCode + " ");
        System.out.println("message params:" + printParams() + " ");
        super.printStackTrace();
    }


    @Override
    public void printStackTrace(PrintStream s)
    {
    	 s.println("error code:" + errorCode + " ");
        s.println("message code:" + messageCode + " ");
        s.println("message params:" + printParams() + " ");
        super.printStackTrace(s);
    }


    @Override
    public void printStackTrace(PrintWriter s)
    {
        s.println("message code:" + messageCode + " ");
        s.println("message params:" + printParams() + " ");
        super.printStackTrace(s);
    }


	@Override
	public String toString() {
		return "ApplicationException [errorCode=" + errorCode + ", messageCode=" + messageCode + ", messageParams="
				+ Arrays.toString(messageParams) + "]";
	}


   

}
