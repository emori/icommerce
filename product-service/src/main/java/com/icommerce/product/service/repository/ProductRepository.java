package com.icommerce.product.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.icommerce.product.service.domain.Product;

public interface ProductRepository extends JpaSpecificationExecutor<Product>,JpaRepository<Product, Long>{

}
