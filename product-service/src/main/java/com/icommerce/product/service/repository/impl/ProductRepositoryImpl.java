package com.icommerce.product.service.repository.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.icommerce.product.service.constants.Constants;
import com.icommerce.product.service.converter.ProductConverter;
import com.icommerce.product.service.domain.Product;
import com.icommerce.product.service.repository.ProductPriceRepository;
import com.icommerce.product.service.repository.ProductRepository;
import com.icommerce.product.service.rest.vm.Pagination;
import com.icommerce.product.service.rest.vm.ProductSearchResponse;
import com.icommerce.product.service.rest.vm.ProductVM;

@Repository
public class ProductRepositoryImpl {

	private final String searchProductHql = "SELECT product FROM Product product LEFT JOIN ProductPrice productPrice ON product.id = productPrice.product.id "
			+ "WHERE productPrice.validFlag = '1' ";

	private final String searchProductCountHql = "SELECT count(*) FROM Product product LEFT JOIN ProductPrice productPrice ON product.id = productPrice.product.id "
			+ "  WHERE productPrice.validFlag = '1'  ";

	@Autowired
	EntityManager entityManager;

	@Autowired
	ProductConverter productConverter;

	@SuppressWarnings("unchecked")
	public ProductSearchResponse<ProductVM> searchProducts(Map<String, String> queryParameters) throws ParseException {

		StringBuilder hqlBuilder = new StringBuilder();
		StringBuilder hqlCountBuilder = new StringBuilder();
		hqlBuilder.append(searchProductHql);
		hqlCountBuilder.append(searchProductCountHql);

		this.createStringBuilderByParams(queryParameters, hqlBuilder, hqlCountBuilder);

		if (StringUtils.isNotBlank(queryParameters.get(Constants.SEARCH_ORDER_BY))) {
			hqlBuilder.append(" ORDER BY "
					+ Constants.SEARCH_COLUMNS_MAPING_MAP.get(queryParameters.get(Constants.SEARCH_ORDER_BY)) + " ");
			if (StringUtils.equalsIgnoreCase(queryParameters.get(Constants.SEARCH_ORDER_TYPE), "DESC")) {
				hqlBuilder.append(Constants.SEARCH_ORDER_DESC);
			} else {
				hqlBuilder.append(Constants.SEARCH_ORDER_ASC);
			}
		} else {
			hqlBuilder.append(" ORDER BY product.createdDate DESC ");
		}

		Query query = entityManager.createQuery(hqlBuilder.toString(), Product.class);
		Query queryCount = entityManager.createQuery(hqlCountBuilder.toString());

		this.addParamsToQuery(queryParameters, query, queryCount);

		Long total = (Long) queryCount.getSingleResult();

		ProductSearchResponse<ProductVM> productSearchResponse = new ProductSearchResponse<>();
		Pagination pagination = new Pagination();

		if (StringUtils.isNotBlank(queryParameters.get(Constants.SEARCH_SIZE))) {
			pagination.setSize(Integer.valueOf(queryParameters.get(Constants.SEARCH_SIZE)));
		} else {
			pagination.setSize(25);
		}

		int firstResult = 0;
		if (StringUtils.isNotBlank(queryParameters.get(Constants.SEARCH_PAGE))) {
			pagination.setPage(Integer.valueOf(queryParameters.get(Constants.SEARCH_PAGE)));
			firstResult = pagination.getPage() == 1 ? 0 : (pagination.getPage() - 1) * pagination.getSize();
		} else {
			pagination.setPage(1);
		}

		pagination.setTotalCount(total);
		productSearchResponse.setPagination(pagination);
		List<ProductVM> listProductVMs = new ArrayList<>();
		if (total != 0) {
			query.setFirstResult(firstResult);
			query.setMaxResults(pagination.getSize());
			List<Product> results = query.getResultList();
			listProductVMs = convertProductsToProductVMForSearchResult(results);
		}

		productSearchResponse.setProducts(listProductVMs);
		return productSearchResponse;

	}

	private void createStringBuilderByParams(Map<String, String> queryParameters, StringBuilder hqlBuilder,
			StringBuilder hqlCountBuilder) {
		queryParameters.forEach((keySearch, valueSearch) -> {
			String searchWithCondition = "";

			switch (keySearch) {
			case Constants.SEARCH_PRODUCT_NAME: {
				if (!StringUtils.isNotBlank(valueSearch))
					break;
				searchWithCondition = " AND product.productName like :productName";
				break;
			}
			case Constants.SEARCH_PRODUCT_CODE: {
				if (!StringUtils.isNotBlank(valueSearch))
					break;

				searchWithCondition = " AND product.productCode = :productCode";

				break;
			}
			case Constants.SEARCH_BRAND: {
				if (!StringUtils.isNotBlank(valueSearch))
					break;

				searchWithCondition = " AND product.brand = :brand";

				break;
			}
			case Constants.SEARCH_FROM_PRICE:
				if (!StringUtils.isNotBlank(valueSearch))
					break;

				searchWithCondition = " AND productPrice.price >= :fromPrice";
				break;
			case Constants.SEARCH_TO_PRICE:
				if (!StringUtils.isNotBlank(valueSearch))
					break;

				searchWithCondition = " AND productPrice.price <= :toPrice";
				break;
			}

			hqlBuilder.append(searchWithCondition);
			hqlCountBuilder.append(searchWithCondition);
		});
	}

	private void addParamsToQuery(Map<String, String> queryParameters, Query query, Query queryCount) {
		queryParameters.forEach((String keySearch, String valueSearch) -> {
			boolean isValueNotBlank = StringUtils.isNotBlank(valueSearch);
			switch (keySearch) {
			case Constants.SEARCH_PRODUCT_CODE:
				if (isValueNotBlank) {
					query.setParameter(Constants.SEARCH_PRODUCT_CODE, valueSearch);
					queryCount.setParameter(Constants.SEARCH_PRODUCT_CODE, valueSearch);
				}
				break;

			case Constants.SEARCH_PRODUCT_NAME:
				if (isValueNotBlank) {
					query.setParameter(Constants.SEARCH_PRODUCT_NAME, "%" + valueSearch + "%" );
					queryCount.setParameter(Constants.SEARCH_PRODUCT_NAME, "%" + valueSearch + "%" );
				}
				break;

			case Constants.SEARCH_BRAND:
				if (isValueNotBlank) {
					query.setParameter(Constants.SEARCH_BRAND, valueSearch);
					queryCount.setParameter(Constants.SEARCH_BRAND, valueSearch);
				}
				break;
			}
		});
	}

	private List<ProductVM> convertProductsToProductVMForSearchResult(List<Product> products) {
		List<ProductVM> productMs = new ArrayList<>();
		for (Product product : products) {
			productMs.add(productConverter.convertToProductVM(product));
		}

		return productMs;
	}
}
