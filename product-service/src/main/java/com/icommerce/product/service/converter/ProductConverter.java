package com.icommerce.product.service.converter;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.icommerce.product.service.constants.Constants;
import com.icommerce.product.service.domain.Product;
import com.icommerce.product.service.rest.vm.ProductVM;

@Component
public class ProductConverter {

	public Product convertToProduct(ProductVM productVM) {
		Product product = new Product();
		BeanUtils.copyProperties(productVM, product);
		return product;
	}
	
	public ProductVM convertToProductVM(Product product) {
		ProductVM productVM = new ProductVM();
		BeanUtils.copyProperties(product, productVM, "productPrices");
		productVM.setPrice(
				product.getProductPrices().stream().
				filter(t -> t.getValidFlag().equals(Constants.VALID_FLAG_VALID)).findFirst().get().getPrice());
		return productVM;
	}
}
