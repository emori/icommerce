package com.icommerce.product.service.config;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class ProxyCustomizer implements RestTemplateCustomizer {
	private final String proxyServerHost;
	private final int proxyServerPort;
	 

	public ProxyCustomizer(String proxyServerHost, int proxyServerPort) {
		super();
		this.proxyServerHost = proxyServerHost;
		this.proxyServerPort = proxyServerPort;
	}


	@Override
    public void customize(RestTemplate restTemplate) {
        HttpHost proxy = new HttpHost(proxyServerHost, proxyServerPort);
        HttpClient httpClient = HttpClientBuilder.create()
            .setRoutePlanner(new DefaultProxyRoutePlanner(proxy) )
            .build();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
    }
}
