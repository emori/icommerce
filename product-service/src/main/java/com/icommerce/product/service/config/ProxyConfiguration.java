package com.icommerce.product.service.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProxyConfiguration {

	@Bean
	@ConditionalOnProperty(value = "application.network.proxy-enable", havingValue = "true", matchIfMissing = false)
	ProxyCustomizer proxyCustomizer(ApplicationProperties applicationProperties) {
		return new ProxyCustomizer(applicationProperties.getNetwork().getProxyUrl(),
				applicationProperties.getNetwork().getProxyPort());
	}
}
