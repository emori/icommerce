package com.icommerce.product.service.utils;

public class StringUtil {

    public static String toString(Object object) {
        if (object != null) {
            return object.toString();
        }
        return null;
    }

    public static String convertUnicodeToCp1258(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }

        String[] CP1258 = {"ỹ", "Ỹ", "ỷ", "Ỷ", "ỵ", "Ỵ", "ỳ", "Ỳ", "ự", "Ự", "ữ", "Ữ", "ử", "Ử", "ừ", "Ừ",
                "ứ", "Ứ", "ủ", "Ủ", "ụ", "Ụ", "ợ", "Ợ", "ỡ", "Ỡ", "ở", "Ở", "ờ", "Ờ", "ớ", "Ớ",
                "ộ", "Ộ", "ỗ", "Ỗ", "ổ", "Ổ", "ồ", "Ồ", "ố", "Ố", "ỏ", "Ỏ", "ọ", "Ọ", "ị", "Ị", "ỉ",
                "Ỉ", "ệ", "Ệ", "ễ", "Ễ", "ể", "Ể", "ề", "Ề", "ế", "Ế", "ẽ", "Ẽ", "ẻ", "Ẻ", "ẹ",
                "Ẹ", "ặ", "Ặ", "ẵ", "Ẵ", "ẳ", "Ẳ", "ằ", "Ằ", "ắ", "Ắ", "ậ", "Ậ", "ẫ", "Ẫ", "ẩ",
                "Ẩ", "ầ", "Ầ", "ấ", "Ấ", "ả", "Ả", "ạ", "Ạ", "ư", "Ư", "ơ", "Ơ", "ũ", "Ũ", "ĩ", "Ĩ",
                "đ", "ă", "Ă", "ý", "ú", "ù", "õ", "ô", "ó", "ò", "í", "ì", "ê", "é", "è", "ã", "â", "á", "à",
                "Ý", "Ú", "Ù", "Õ", "Ô", "Ó", "Ò", "Đ", "Í", "Ì", "Ê", "É", "È", "Ã", "Â", "Á", "À"};
        String[] Unicode_char = {"\u1EF9", "\u1EF8", "\u1EF7", "\u1EF6", "\u1EF5", "\u1EF4", "\u1EF3", "\u1EF2",
                "\u1EF1", "\u1EF0", "\u1EEF", "\u1EEE", "\u1EED", "\u1EEC", "\u1EEB", "\u1EEA", "\u1EE9", "\u1EE8",
                "\u1EE7", "\u1EE6", "\u1EE5", "\u1EE4", "\u1EE3", "\u1EE2", "\u1EE1", "\u1EE0", "\u1EDF", "\u1EDE",
                "\u1EDD", "\u1EDC", "\u1EDB", "\u1EDA", "\u1ED9", "\u1ED8", "\u1ED7", "\u1ED6", "\u1ED5", "\u1ED4",
                "\u1ED3", "\u1ED2", "\u1ED1", "\u1ED0", "\u1ECF", "\u1ECE", "\u1ECD", "\u1ECC", "\u1ECB", "\u1ECA",
                "\u1EC9", "\u1EC8", "\u1EC7", "\u1EC6", "\u1EC5", "\u1EC4", "\u1EC3", "\u1EC2", "\u1EC1", "\u1EC0",
                "\u1EBF", "\u1EBE", "\u1EBD", "\u1EBC", "\u1EBB", "\u1EBA", "\u1EB9", "\u1EB8", "\u1EB7", "\u1EB6",
                "\u1EB5", "\u1EB4", "\u1EB3", "\u1EB2", "\u1EB1", "\u1EB0", "\u1EAF", "\u1EAE", "\u1EAD", "\u1EAC",
                "\u1EAB", "\u1EAA", "\u1EA9", "\u1EA8", "\u1EA7", "\u1EA6", "\u1EA5", "\u1EA4", "\u1EA3", "\u1EA2",
                "\u1EA1", "\u1EA0", "\u01B0", "\u01AF", "\u01A1", "\u01A0", "\u0169", "\u0168", "\u0129", "\u0128",
                "\u0111", "\u0103", "\u0102", "\u00FD", "\u00FA", "\u00F9", "\u00F5", "\u00F4", "\u00F3", "\u00F2",
                "\u00ED", "\u00EC", "\u00EA", "\u00E9", "\u00E8", "\u00E3", "\u00E2", "\u00E1", "\u00E0", "\u00DD",
                "\u00DA", "\u00D9", "\u00D5", "\u00D4", "\u00D3", "\u00D2", "\u0110", "\u00CD", "\u00CC", "\u00CA",
                "\u00C9", "\u00C8", "\u00C3", "\u00C2", "\u00C1", "\u00C0"};

        str = replaceString(str, Unicode_char, CP1258);
        return str;
    }

    public static String convertCp1258ToUnicode(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }

        String[] CP1258 = {"ỹ", "Ỹ", "ỷ", "Ỷ", "ỵ", "Ỵ", "ỳ", "Ỳ", "ự", "Ự", "ữ", "Ữ", "ử", "Ử", "ừ", "Ừ",
                "ứ", "Ứ", "ủ", "Ủ", "ụ", "Ụ", "ợ", "Ợ", "ỡ", "Ỡ", "ở", "Ở", "ờ", "Ờ", "ớ", "Ớ",
                "ộ", "Ộ", "ỗ", "Ỗ", "ổ", "Ổ", "ồ", "Ồ", "ố", "Ố", "ỏ", "Ỏ", "ọ", "Ọ", "ị", "Ị", "ỉ",
                "Ỉ", "ệ", "Ệ", "ễ", "Ễ", "ể", "Ể", "ề", "Ề", "ế", "Ế", "ẽ", "Ẽ", "ẻ", "Ẻ", "ẹ",
                "Ẹ", "ặ", "Ặ", "ẵ", "Ẵ", "ẳ", "Ẳ", "ằ", "Ằ", "ắ", "Ắ", "ậ", "Ậ", "ẫ", "Ẫ", "ẩ",
                "Ẩ", "ầ", "Ầ", "ấ", "Ấ", "ả", "Ả", "ạ", "Ạ", "ư", "Ư", "ơ", "Ơ", "ũ", "Ũ", "ĩ", "Ĩ",
                "đ", "ă", "Ă", "ý", "ú", "ù", "õ", "ô", "ó", "ò", "í", "ì", "ê", "é", "è", "ã", "â", "á", "à",
                "Ý", "Ú", "Ù", "Õ", "Ô", "Ó", "Ò", "Đ", "Í", "Ì", "Ê", "É", "È", "Ã", "Â", "Á", "À"};
        String[] Unicode_char = {"\u1EF9", "\u1EF8", "\u1EF7", "\u1EF6", "\u1EF5", "\u1EF4", "\u1EF3", "\u1EF2",
                "\u1EF1", "\u1EF0", "\u1EEF", "\u1EEE", "\u1EED", "\u1EEC", "\u1EEB", "\u1EEA", "\u1EE9", "\u1EE8",
                "\u1EE7", "\u1EE6", "\u1EE5", "\u1EE4", "\u1EE3", "\u1EE2", "\u1EE1", "\u1EE0", "\u1EDF", "\u1EDE",
                "\u1EDD", "\u1EDC", "\u1EDB", "\u1EDA", "\u1ED9", "\u1ED8", "\u1ED7", "\u1ED6", "\u1ED5", "\u1ED4",
                "\u1ED3", "\u1ED2", "\u1ED1", "\u1ED0", "\u1ECF", "\u1ECE", "\u1ECD", "\u1ECC", "\u1ECB", "\u1ECA",
                "\u1EC9", "\u1EC8", "\u1EC7", "\u1EC6", "\u1EC5", "\u1EC4", "\u1EC3", "\u1EC2", "\u1EC1", "\u1EC0",
                "\u1EBF", "\u1EBE", "\u1EBD", "\u1EBC", "\u1EBB", "\u1EBA", "\u1EB9", "\u1EB8", "\u1EB7", "\u1EB6",
                "\u1EB5", "\u1EB4", "\u1EB3", "\u1EB2", "\u1EB1", "\u1EB0", "\u1EAF", "\u1EAE", "\u1EAD", "\u1EAC",
                "\u1EAB", "\u1EAA", "\u1EA9", "\u1EA8", "\u1EA7", "\u1EA6", "\u1EA5", "\u1EA4", "\u1EA3", "\u1EA2",
                "\u1EA1", "\u1EA0", "\u01B0", "\u01AF", "\u01A1", "\u01A0", "\u0169", "\u0168", "\u0129", "\u0128",
                "\u0111", "\u0103", "\u0102", "\u00FD", "\u00FA", "\u00F9", "\u00F5", "\u00F4", "\u00F3", "\u00F2",
                "\u00ED", "\u00EC", "\u00EA", "\u00E9", "\u00E8", "\u00E3", "\u00E2", "\u00E1", "\u00E0", "\u00DD",
                "\u00DA", "\u00D9", "\u00D5", "\u00D4", "\u00D3", "\u00D2", "\u0110", "\u00CD", "\u00CC", "\u00CA",
                "\u00C9", "\u00C8", "\u00C3", "\u00C2", "\u00C1", "\u00C0"};

        str = replaceString(str, CP1258, Unicode_char);
        return str;
    }

    public static String convertToSearch(String str) {
        String[] inputString = {"a", "ă", "â",
                "e", "ê", "i",
                "o", "ô", "ơ",
                "u", "ư",
                "y",

                "ạ", "ặ", "ậ",
                "ẹ", "ệ",
                "ị",
                "ọ", "ộ", "ợ",
                "ụ", "ự", "ỵ",

                "à", "ằ", "ầ",
                "è", "ề",
                "ì",
                "ò", "ồ", "ờ",
                "ù", "ừ",
                "ỳ",

                "á", "ắ", "ấ",
                "é", "ế", "í",
                "ó", "ố", "ớ",
                "ú", "ứ",
                "ý",

                "ã", "ẵ", "ẫ",
                "ẽ", "ễ",
                "ĩ",
                "õ", "ỗ", "ỡ",
                "ũ", "ữ",
                "ỹ",

                "ả", "ẳ", "ẩ",
                "ẻ", "ể",
                "ỉ",
                "ỏ", "ổ", "ở",
                "ủ", "ử",
                "ỷ"};

        String[] patternString = {"(a|ă|â|a|ặ|ậ|à|ằ|ầ|á|ắ|ấ|ã|ẵ|ẫ|ả|ẳ|ẩ)", "(a|ă|â|a|ặ|ậ|à|ằ|ầ|á|ắ|ấ|ã|ẵ|ẫ|ả|ẳ|ẩ)", "(a|ă|â|a|ặ|ậ|à|ằ|ầ|á|ắ|ấ|ã|ẵ|ẫ|ả|ẳ|ẩ)",
                "(e|ê|ẹ|ệ|è|ề|é|ế|ẽ|ễ|ẻ|ể)", "(e|ê|ẹ|ệ|è|ề|é|ế|ẽ|ễ|ẻ|ể)",
                "(i|ị|ì|í|ĩ|ỉ)",
                "(o|ô|ơ|ọ|ộ|ợ|ò|ồ|ờ|ó|ố|ớ|õ|ỗ|ỡ|ỏ|ổ|ở)", "(o|ô|ơ|ọ|ộ|ợ|ò|ồ|ờ|ó|ố|ớ|õ|ỗ|ỡ|ỏ|ổ|ở)", "(o|ô|ơ|ọ|ộ|ợ|ò|ồ|ờ|ó|ố|ớ|õ|ỗ|ỡ|ỏ|ổ|ở)",
                "(u|ư|ụ|ự|ù|ừ|ú|ứ|ũ|ữ|ủ|ử)", "(u|ư|ụ|ự|ù|ừ|ú|ứ|ũ|ữ|ủ|ử)",
                "(y|ỵ|ỳ|ý|ỹ|ỷ)",
                "(a|ặ|ậ)", "ặ", "ậ",
                "(ẹ|ệ)", "ệ",
                "ị",
                "(ọ|ộ|ợ)", "ộ", "ợ",
                "(ụ|ự)", "ự",
                "ỵ",
                "(à|ằ|ầ)", "ằ", "ầ",
                "(è|ề)", "ề",
                "ì",
                "(ò|ồ|ờ)", "ồ", "ờ",
                "(ù|ừ)", "ừ",
                "ỳ",
                "(á|ắ|ấ)", "ắ", "ấ",
                "(é|ế)", "ế",
                "í",
                "(ó|ố|ớ)", "ố", "ớ",
                "(ú|ứ)", "ứ",
                "ý",
                "(ã|ẵ|ẫ)", "ẵ", "ẫ",
                "(ẽ|ễ)", "ễ",
                "ĩ",
                "(õ|ỗ|ỡ)", "ỗ", "ỡ",
                "(ũ|ữ)", "ữ",
                "ỹ",
                "(ả|ẳ|ẩ)", "ẳ", "ẩ",
                "(ẻ|ể)", "ể",
                "ỉ",
                "(ỏ|ổ|ở)", "ổ", "ở",
                "(ủ|ử)", "ử",
                "ỷ"};

        StringBuffer result = new StringBuffer();
        int startIndex = 0;
        while (startIndex < str.length()) {
            boolean isFound = false;
            for (int index = 0; index < inputString.length; index++) {
                int foundIndex = str.indexOf(inputString[index], startIndex);
                if (foundIndex >= 0) {
                    result.append(str.substring(startIndex, foundIndex));
                    result.append(patternString[index]);
                    startIndex = foundIndex;
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                result.append(str.substring(startIndex, startIndex + 1));
            }
            ++startIndex;
        }

        return result.toString();
    }

    private static String replaceString(String text, final String pattern, final String replace) {
        int startIndex = 0, foundIndex;
        StringBuffer result = new StringBuffer();

        // Look for a pattern to replace
        while ((foundIndex = text.indexOf(pattern, startIndex)) >= 0) {
            result.append(text.substring(startIndex, foundIndex));
            result.append(replace);
            startIndex = foundIndex + pattern.length();
        }
        result.append(text.substring(startIndex));
        return result.toString();
    }

    // String replacement methods
    private static String replaceString(String text, final String[] pattern, final String[] replace) {
        int startIndex, foundIndex;
        StringBuffer result = new StringBuffer();

        for (int i = 0; i < pattern.length; i++) {
            startIndex = 0;
            result.setLength(0); // clear the buffer
            // Look for a pattern to replace
            while ((foundIndex = text.indexOf(pattern[i], startIndex)) >= 0) {
                result.append(text.substring(startIndex, foundIndex));
                result.append(replace[i]);
                startIndex = foundIndex + pattern[i].length();
            }
            result.append(text.substring(startIndex));
            text = result.toString();
        }
        return text;
    }
}
