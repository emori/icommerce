package com.icommerce.product.service.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icommerce.product.service.constants.Constants;
import com.icommerce.product.service.converter.ProductConverter;
import com.icommerce.product.service.domain.Product;
import com.icommerce.product.service.domain.ProductPrice;
import com.icommerce.product.service.repository.ProductPriceRepository;
import com.icommerce.product.service.repository.ProductRepository;
import com.icommerce.product.service.repository.impl.ProductRepositoryImpl;
import com.icommerce.product.service.rest.vm.ProductSearchResponse;
import com.icommerce.product.service.rest.vm.ProductVM;
import com.icommerce.product.service.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductPriceRepository productPriceRepository;

	@Autowired
	private ProductRepositoryImpl productRepositoryImpl;
	@Autowired
	private ProductConverter productConverter;

	@Override
	public Product addProduct(ProductVM productVM) {
		Product product = productConverter.convertToProduct(productVM);
		product.setValidFlag(Constants.VALID_FLAG_VALID);
		productRepository.save(product);
		ProductPrice productPrice = new ProductPrice();
		productPrice.setProduct(product);
		productPrice.setPrice(productVM.getPrice());
		productPrice.setValidFlag(Constants.VALID_FLAG_VALID);
		productPriceRepository.save(productPrice);
		return product;
	}

	@Override
	public List<ProductVM> listProduct() {
		List<Product> products = productRepository.findAll();
		List<ProductVM> productVMs = new ArrayList<>();
		for (Product product : products) {
			productVMs.add(productConverter.convertToProductVM(product));
		}
		return productVMs;
	}

	@Override
	public ProductSearchResponse<ProductVM> search(Map<String, String> queryParameters) throws ParseException {
		return productRepositoryImpl.searchProducts(queryParameters);
	}

}
