package com.icommerce.product.service.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.icommerce.product.service.domain.Product;
import com.icommerce.product.service.rest.vm.ProductSearchResponse;
import com.icommerce.product.service.rest.vm.ProductVM;

public interface ProductService {

	Product addProduct(ProductVM productVM);

	List<ProductVM> listProduct();

	ProductSearchResponse<ProductVM> search(Map<String, String> queryParameters) throws ParseException;
}
