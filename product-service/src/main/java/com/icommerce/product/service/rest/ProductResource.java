package com.icommerce.product.service.rest;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.icommerce.product.service.rest.vm.ProductVM;
import com.icommerce.product.service.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductResource {

	@Autowired
	private ProductService productService;
	
	@PostMapping(value = "/product/add")
	public Object addProduct(@RequestBody() ProductVM product, HttpServletRequest request){
		return this.productService.addProduct(product);
	}
	
	@GetMapping(value = "/product/list")
	public Object listProduct(HttpServletRequest request){
		return this.productService.listProduct();
	}
	
	@GetMapping(value = "/product/search")
	public Object search(@RequestParam Map<String, String> queryParameters, HttpServletRequest request) throws ParseException{
		return this.productService.search(queryParameters);
	}
}
