package com.icommerce.product.service.utils;

import com.icommerce.product.service.constants.MessageConstants;
import com.icommerce.product.service.exception.ApplicationException;
import com.icommerce.product.service.exception.BusinessException;

public class ExceptionUtil {

	
	public static BusinessException invalidMobileNumber() {
		return new BusinessException(MessageConstants.INVALID_MOBILE_NUMBER);
	}

	public static ApplicationException accessDenied(String message) {
		return new ApplicationException(403, MessageConstants.ACCESS_DENIED, new Object[] { message }, null);
	}

	public static ApplicationException applicationException(int status, String message, Exception e) {
		return new ApplicationException(status, message, null, e);
	}

	public static ApplicationException applicationException(int status, String message) {
		return new ApplicationException(status, message, null, null);
	}

	public static ApplicationException applicationException(int status, String message, Object responseBody) {
		return new ApplicationException(status, message, null, responseBody, null);
	}
}
