package com.icommerce.product.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.icommerce.product.service.domain.ProductPrice;

public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {

}
