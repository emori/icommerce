This project is to demo a backend of icommerce application using microservice architecture 

There are 3 projects
1. config-service: Spring cloud server to centralize all configuration. This project should be run first.

Use below command to run this project in local
mvn spring-boot:run

2. icommerce-backend: Spring boot application which is the back-nd of icommerce site. All requests from Internet will go to this backend service

Use below command to run this project in local
mvn spring-boot:run

3. product-service: Spring boot application, microservice to manage products

Use below command to run this project in local
mvn spring-boot:run


## Infrastructure diagram for PRD

Demo-iCommerce.png

## Demo API calls
#Add a new product
curl -d '{"productName" : "XBOX", "productCode":"XBOX", "brand": "Microsoft", "price": 1000000}' -H "Content-Type: application/json" -X POST http://localhost:8081/product-service/api/product/add

#List all products
curl http://localhost:8081/product-service/api/product/list

#Search product by product name

1. Call directly to product-service
curl http://localhost:8081/product-service/api/product/search?productName=XBOX

2. Call via Zuul proxy

curl http://localhost:8080/product-service/api/product/search?productName=XBOX

